import React from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import Users from './component/Users/Users/UsersComponent';
import Register from './component/Users/Register/RegisterComponent';
import Tasks from './component/Tasks/Tasks/TasksComponent';
import Assign from './component/Tasks/Assign/AssignComponent';
import Common from './component/Tasks/Common/CommonComponent';

function App() {
  return (<Router>
    <div className="App">
      <header>
        <nav className="navbar navbar-expand-md navbar-dark bg-dark">
          <h3 className="navbar-brand">Orenda Test</h3>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item active">
                <Link className="nav-link" to={"/users"}>Users</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to={"/register"}>Register</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to={"/common"}>Tasks</Link>
              </li>
            </ul>
          </div>
        </nav>
      </header>

      <main role="main" className="flex-shrink-0 mt-auto py-3">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <Switch>
                <Route exact path='/' component={Users} />
                <Route path="/users" component={Users} />
                <Route path="/register" component={Register} />
                <Route path="/common" component={Common} />
                <Route path="/tasks/:email" component={Tasks} />
                <Route path="/assign/:email" component={Assign} />
              </Switch>
            </div>
          </div>
        </div>
      </main>
    </div>
  </Router>
  );
}

export default App;
