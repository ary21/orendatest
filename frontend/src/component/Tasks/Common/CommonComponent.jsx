import React, { Component } from 'react';
import axios from 'axios';
import MainUri from '../../../Context';
import DataTable from './commonTable';

export default class CommonComponent extends Component {
    
    constructor(props) {
        super(props);

        this.handleSelect = this.handleSelect.bind(this);
        this.getCommonTasks = this.getCommonTasks.bind(this);

        this.state = { 
            tasksCollection: [],
            usersCollection: [],
            usersSelected: []
        };
    }

    componentDidMount() {
        axios.get(`${MainUri}/api/users`)
            .then(res => {
                this.setState({ usersCollection: res.data.data });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    dataUserTable() {
        return this.state.usersCollection.map((data, i) => {
            return (
                <tr key={i}>
                    <td>
                        <input type="checkbox" value={data.email} onClick={this.handleSelect} className="mr-2"/>
                        {data.email}
                    </td>
                </tr>
            );
        });
    }

    dataTable() {
        return this.state.tasksCollection.map((data, i) => {
            return <DataTable obj={data} key={i} />;
        });
    }

    handleSelect(e) {
        const value = e.target.value;

        if (e.target.checked) {
            this.state.usersSelected.push(value);
        } else {
            const newSelectedUser = this.state.usersSelected.filter(x => x != value)
            this.setState({ usersSelected: newSelectedUser });
        }
    }

    getCommonTasks() {
        const selected = this.state.usersSelected;
        this.setState({ tasksCollection: [] });
        this.dataTable()

        if (selected.length) {
            axios.get(`${MainUri}/api/tasks/common`, 
                    { params: { user: selected } },
                )
                .then(res => {
                    this.setState({ tasksCollection: res.data.tasks });
                    this.dataTable()
                })
                .catch(function (error) {
                    console.log(error);
                })
        }
        else 
            alert('Please select user')
    }

    render() {
        return (
            <div className="wrapper-users">
                <div className="container">
                    <div className="box">
                        <div className="box-header"><h3>Common Tasks</h3></div>
                        <div className="box-body">
                            <div className="row">
                                <div className="col-md-6">
                                    <div>
                                        <h4 className="float-left">Select User</h4>
                                        <button className="btn btn-sm btn-primary float-right" onClick={this.getCommonTasks}>Check Tasks</button>
                                    </div>
                                    <table className="table table-bordered">
                                        <thead className="thead-dark">
                                            <tr>
                                                <th>Email</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.dataUserTable()}
                                        </tbody>
                                    </table>
                                </div>
                                <div className="col-md-6">
                                    <h4 className="float-left">All Common Tasks</h4>
                                    <table className="table table-bordered">
                                        <thead className="thead-dark">
                                            <tr>
                                                <th>Tasks</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.dataTable()}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}