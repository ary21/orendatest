import React, { Component } from 'react';
import { Link } from "react-router-dom";

class DataTable extends Component {
    render() {
        return (
            <tr>
                <td>
                    {this.props.obj}
                </td>
                <td>
                    <Link to={`/unassign/"${this.props.obj}"`} className="btn btn-sm btn-primary">Unassign</Link>
                </td>
            </tr>
        );
    }
}

export default DataTable;