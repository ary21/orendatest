import React, { Component } from 'react';
import axios from 'axios';
import { Link } from "react-router-dom";

import DataTable from './tasksTable';
import MainUri from '../../../Context';

export default class TasksComponent extends Component {
    
    constructor(props) {
        super(props);
        
        this.state = {
            tasksCollection: [],
            email: this.props.match.params.email
        }
    }

    componentDidMount() {
        axios.get(`${MainUri}/api/tasks/common`, 
            { params: { user: [this.state.email] } },
        )
        .then(res => {
            this.setState({ tasksCollection: res.data.tasks });
        })
        .catch(function (error) {
            console.log(error);
        })
    }

    dataTable() {
        return this.state.tasksCollection.map((data, i) => {
            return <DataTable obj={data} key={i} />;
        });
    }

    render() {
        return (
            <div className="wrapper-users">
                <div className="container">
                    <div className="box">
                        <div className="box-header"><h3>Tasks : {this.state.email}</h3></div>
                        <div className="box-body">
                            <div className="row">
                                <div className="col-md-12 mb-3">
                                    <Link to={`/users`} className="btn btn-danger mr-2">Back</Link>
                                    <Link to={`/assign/${this.state.email}`} className="btn btn-primary">Assign new task</Link>
                                </div>
                                <div className="col-md-12">
                                    <table className="table table-bordered">
                                        <thead className="thead-dark">
                                            <tr>
                                                <th>Tasks</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.dataTable()}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}