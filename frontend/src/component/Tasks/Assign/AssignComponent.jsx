import React, { Component } from 'react';
import axios from 'axios';
import { Link } from "react-router-dom";

import MainUri from '../../../Context';

export default class RegisterComponent extends Component {

    constructor(props) {
        super(props)
        
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        console.log('this.props', this.props);
        
        this.state = {
            email: this.props.match.params.email,
            task: ''
        }
    }

    handleChange(e) {
        this.setState({ task: e.target.value })
    }

    handleSubmit(e) {
        e.preventDefault()
        
        if (this.state.task) {
            axios.post(`${MainUri}/api/assign`, 
                { user: this.state.email, tasks: [ this.state.task ] })
            .then((res) => {
                console.log(res.data)
                alert(res.data.messages)
            }).catch((error) => {
                console.log(error)
            });
    
            this.setState({ task: '' })
        } else
            alert('Entry email please')
    }

    render() {
        return (
            <div className="wrapper">
                <div className="box">
                    <div className="box-header">
                        <h3>Assign User's Task : {this.state.email}</h3>
                    </div>
                    <div className="box-body">
                        <form className="form form-task" onSubmit={this.handleSubmit}>
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <label htmlFor="task">Task</label>
                                        <input type="text" id="task" placeholder="Entry task here" value={this.state.task} onChange={this.handleChange} className="form-control" />
                                    </div>
                                    <div className="form-group">
                                        <Link to={`/users`} className="btn btn-danger">Back</Link>
                                        <input type="submit" value="Submit" className="btn btn-success float-right"/>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}