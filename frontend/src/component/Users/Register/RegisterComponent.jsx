import React, { Component } from 'react';
import axios from 'axios';

import MainUri from '../../../Context';

export default class RegisterComponent extends Component {

    constructor(props) {
        super(props)
        
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {
            email: ''
        }
    }

    handleChangeEmail(e) {
        this.setState({ email: e.target.value })
    }

    handleSubmit(e) {
        e.preventDefault()
        
        if (this.state.email) {
            axios.post(`${MainUri}/api/register`, 
                { users: [ this.state.email ] })
            .then((res) => {
                console.log(res.data)
                alert(res.data.messages)
            }).catch((error) => {
                console.log(error)
            });
    
            this.setState({ email: '' })
        } else
            alert('Entry email please')
    }

    render() {
        return (
            <div className="wrapper">
                <div className="box">
                    <div className="box-header">
                        <h3>Register User</h3>
                    </div>
                    <div className="box-body">
                        <form className="form form-user" onSubmit={this.handleSubmit}>
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <label htmlFor="email">Email</label>
                                        <input type="email" id="email" placeholder="Entry email here" value={this.state.email} onChange={this.handleChangeEmail} className="form-control" />
                                    </div>
                                    <div className="form-group">
                                        <input type="submit" value="Submit" className="btn btn-success btn-block" />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}