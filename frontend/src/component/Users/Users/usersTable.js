import React, { Component } from 'react';
import { Link } from "react-router-dom";

class DataTable extends Component {
    render() {
        return (
            <tr>
                <td>
                    {this.props.obj.email}
                </td>
                <td>
                    <Link to={`/assign/${this.props.obj.email}`} className="btn btn-sm btn-primary mr-2">Assign new task</Link>
                    <Link to={`/tasks/${this.props.obj.email}`} className="btn btn-sm btn-primary">All Task</Link>
                </td>
            </tr>
        );
    }
}

export default DataTable;