const 
    express = require('express'),
    path = require('path'),
    cookieParser = require('cookie-parser'),
    cors = require('cors'),
    db = require('./models'),
    logger = require('morgan'),
    app = express();

const 
    indexRouter = require('./routes/index'),
    apiRouter = require('./routes/api');

require('dotenv').config();
const port = process.env.PORT || 3000;

db.sequelize.sync().then(() => {
    console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
    console.log(`~~ DB connected. Running on http://localhost:${port}/ ~~`);
    console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
})

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

app.use('/', indexRouter);
app.use('/api', apiRouter);

module.exports = app;
