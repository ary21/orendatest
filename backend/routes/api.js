const
  express = require('express'),
  router = express.Router();

const model = require('../models');

/* GET for /api */
router.get('/', function (req, res, next) {
  res.send('api');
})

/* GET users listing. */
router.get('/users', async function(req, res, next) {
  try {
    const users = await model.users.findAll({ attributes: ['email'] });
    if (users && users.length) {
      res.status(200).json({
        'status': 'OK',
        'messages': 'All users',
        'data': users
      })
    } else {
      res.status(302).json({
        'status': 'ERROR',
        'messages': 'EMPTY',
        'data': {}
      })
    }
  } catch (err) {
    console.log('err catch', err);
    res.status(400).json(err)
  }
});

/* POST new user */
router.post('/register', async function(req, res, next) {
  try {
    if (req.body.users) {
      const bodyUsers = req.body.users;

      /* validation */
      const promises = bodyUsers.map(async(elm) => { 
        const findUser = await model.users.findAll({ where: { email: elm } })
        return (findUser && findUser.length > 0) ? null : elm;
      })
    
      let validBody = await Promise.all(promises);
      validBody = validBody.filter(el => { return el != null });
      
      const remakeBody = validBody.map(elm => { return { email: elm } });

      await model.users.bulkCreate(remakeBody, { returning: true })
      .then(newUsers => {
        res.status(200).json({
          'status': 'OK',
          'messages': 'Success insert',
          'data': validBody,
        })
      })
      .catch(er => {
        if (er) {
          console.log('er catch', er);
          res.status(400).json(er)
        }
      })
    } else {
      res.status(400).json({
        'status': 'ERROR',
        'messages': 'Request not valid',
        'data': {}
      })
    }
 } catch (err) {
  console.log('err catch', err);
  res.status(400).json(err)
 }
});

/* POST tasks asign to user */
router.post('/assign', async function(req, res, next) {
  try {
    if (req.body.user && req.body.tasks) {
      const thisUser = await model.users.findAll({ plain:true, where: { email: req.body.user } });
      
      if (thisUser) {
        const bodyTasks = req.body.tasks;
        
        /* validation */
        const promises = bodyTasks.map(async(elm) => { 
          const findTask = await model.tasks.findAll({ where: { id_user: thisUser.id, task: elm }  })
          return (findTask && findTask.length > 0) ? null : elm;
        })
      
        let validBody = await Promise.all(promises);
        validBody = validBody.filter(el => { return el != null });
        
        const remakeBody = validBody.map(elm => { 
          return { id_user: thisUser.id, task: elm } 
        });
        
        await model.tasks.bulkCreate(remakeBody, { returning: true })
        .then(newTasks => {
          res.status(200).json({
            'status': 'OK',
            'messages': 'Success insert',
            'data': validBody,
          })
        })
        .catch(er => {
          if (er) {
            console.log('er catch', er);
            res.status(400).json(er)
          }
        })
      } else {
        res.status(400).json({
          'status': 'ERROR',
          'messages': 'User not found',
          'data': {}
        })
      }
    } else {
      res.status(400).json({
        'status': 'ERROR',
        'messages': 'Request not valid',
        'data': {}
      })
    }
 } catch (err) {
  console.log('err catch', err);
  res.status(400).json(err)
 }
});

/* POST tasks unassign from user */
router.post('/unassign', async function(req, res, next) {
  try {
    if (req.body.user && req.body.tasks) {
      const thisUser = await model.users.findAll({
        plain:true,
        where: { email: req.body.user }
      });
      
      if (thisUser) {
        const bodyTasks = req.body.tasks;

        await model.tasks.destroy({
          where: { id_user: thisUser.id, task: bodyTasks }
        })
        .then(newTasks => {
          res.status(200).json({
            'status': 'OK',
            'messages': 'Success remove',
            'data': bodyTasks,
          })
        })
        .catch(er => {
          if (er) {
            console.log('er catch', er);
            res.status(400).json(er)
          }
        });
      } else {
        res.status(400).json({
          'status': 'ERROR',
          'messages': 'User not found',
          'data': {}
        })
      }
    } else {
      res.status(400).json({
        'status': 'ERROR',
        'messages': 'Request not valid',
        'data': {}
      })
    }
 } catch (err) {
  console.log('err catch', err);
  res.status(400).json(err)
 }
});

/* GET tasks listing. */
router.get('/tasks/common', async function(req, res, next) {
  try {
    let tasks = getTasks = [];
    if (req.query) {
      const checkUsers = req.query.user;
      if (Array.isArray(checkUsers) || (checkUsers.constructor === Object)) {
        for (let i = 0; i < checkUsers.length; i++) {
          const elm = checkUsers[i];
          
          const thisUser = await model.users.findAll({
            plain:true,
            where: { email: elm }
          });

          if (thisUser) {
            const thisTasks = await model.tasks.findAll({
              raw: true,
              attributes: ['task'],
              where: { id_user: thisUser.id }
            });

            if (thisTasks)
              getTasks.push(...thisTasks);
          }
        }
        
        if (getTasks)
          tasks = getTasks.map((t) => t.task );

        if (tasks && tasks.length) {
          res.status(200).json({ 'tasks': tasks })
        } else {
          res.status(302).json({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'data': {}
          })
        }
      } else {
        res.status(400).json({
          'status': 'ERROR',
          'messages': 'Request not valid',
          'data': {}
        })
      }
    } else {
      res.status(400).json({
        'status': 'ERROR',
        'messages': 'Request not valid',
        'data': {}
      })
    }
  } catch (err) {
    console.log('err catch', err);
    res.status(400).json(err)
  }
});

module.exports = router;
