# Project for Orenda Preliminary Test


## Built With
### Backend
* NodeJS
* Framework : ExpressJS with 'express-generator'
* Database : MySQL with 'mysql2'
* ORM : Sequalize

### Frontend
* ReactJS
* axios

___


## Guide for running local
- clone project
- open project with command app like cmd, terminal, konsole, etc
- make sure inside project folder or go to inside project folder run
    > cd orendatest

### Backend
- go to backend folder
    > cd backend
- run install
    > npm install
- create database
- copy file **.env-example** to **.env** file
- open .env file and set database connection and port if need
- do migration with run
    > sequelize db:migrate
- if migration failed, just create database and import **orendatest.sql** manually use tools like phpmyadmin, mysql workbench, etc
- run start
    > npm start
- or run this for development 
    > npm run dev
- ready to use API :

#### Sample to use API
* for all users : http://localhost:3030/api/users
* for register new users : http://localhost:3030/api/register
    * Sample request
        > { "users": [ "eko@email.com", "tono@email.com", "joko@email.com" ] }
    
* for assign tasks to user : http://localhost:3030/api/assign
    * Sample request
        > {
        >   "user": "eko@email.com", 
        >   "tasks": [ "Buy drink", "Buy pen" ] 
        > }

* for unassign task from user : http://localhost:3030/api/unassign
    * Sample request
        > {
        >   "user": "eko@email.com", 
        >   "tasks": [ "Buy drink" ]
        > }

* for tasks : http://localhost:3030/api/tasks/common
    * Sample request
        > { "user": [ "eko@email.com", "joko@email.com" ] } : for all tasks some users

___

### Frontend
- go to backend folder
    > cd frontend
- run install
    > npm install
- run start
    > yarn start
- open url with browser
    > http://localhost:3000